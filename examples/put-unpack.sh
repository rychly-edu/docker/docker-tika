#!/bin/sh

# https://cwiki.apache.org/confluence/display/TIKA/TikaServer#TikaServer-UnpackResource

FILE_PATH="${1}"
FILE_NAME="${FILE_PATH##*/}"
FILE_TYPE=$(file --dereference --brief --mime-type "${FILE_PATH}")

URL="http://localhost:9998/unpack"

OUT="${2}"
if [ -z "${OUT}" ]; then
	OUT="application/zip"
	#OUT="application/x-tar"
fi

EXT2=${OUT##*/} EXT=${EXT2#*-}
FILE="unpacked.${EXT}"

echo "### unpacked content will be saved into file ${FILE}" >&2

exec curl -X PUT -H "Accept: ${OUT}" --data-binary "@${FILE_PATH}" -H "Content-Disposition: attachment; filename=${FILE_NAME}" -H "Content-Type: ${FILE_TYPE}" "${URL}" > "${FILE}"
