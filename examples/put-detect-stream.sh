#!/bin/sh

# https://cwiki.apache.org/confluence/display/TIKA/TikaServer#TikaServer-DetectorResource

FILE_PATH="${1}"
FILE_NAME="${FILE_PATH##*/}"

URL="http://localhost:9998/detect/stream"

exec curl -i -X PUT --data-binary "@${FILE_PATH}" -H "Content-Disposition: attachment; filename=${FILE_NAME}" -H "Content-Type: ${FILE_TYPE}" "${URL}"
