#!/bin/sh

# https://cwiki.apache.org/confluence/display/TIKA/TikaServer#TikaServer-LanguageResource

UTF8_TEXT="${1}"

URL="http://localhost:9998/language/string"

exec curl -i -X PUT --data "${UTF8_TEXT}" "${URL}"
