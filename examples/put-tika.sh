#!/bin/sh

# https://cwiki.apache.org/confluence/display/TIKA/TikaServer#TikaServer-TikaResource

FILE_PATH="${1}"
FILE_NAME="${FILE_PATH##*/}"
FILE_TYPE=$(file --dereference --brief --mime-type "${FILE_PATH}")

URL="http://localhost:9998/tika"

OUT="${2}"
if [ -z "${OUT}" ]; then
	# HTML is better for a human-readable view
	OUT="text/html"
	# plain-text is better for subsequent processing
	#OUT="text/plain"
fi

exec curl -i -X PUT -H "Accept: ${OUT}" --data-binary "@${FILE_PATH}" -H "Content-Disposition: attachment; filename=${FILE_NAME}" -H "Content-Type: ${FILE_TYPE}" "${URL}"
